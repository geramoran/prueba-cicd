import React from 'react'

function ToDoTitle() {
  return (
    <div>
      <b style={{
        fontSize: '30px',
        textAlign: 'center'
      }}>Listado de tareas</b>
    </div>
  )
}

export {ToDoTitle}
