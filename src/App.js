import { ToDoCount } from './ToDoCount';
import { ToDoTitle } from './ToDoTitle';
import { ToDoList } from './ToDoList';
import { ToDoSearch } from './ToDoSearch';
import { BtnToDo } from './BtnToDo';
import { ToDoItem } from './ToDoItem';
import './App.css';
import React from 'react';

const arrayToDos = [
  {text: 'Cortar el pelo', complete: true},
  {text: 'Cortar la papa', complete: false},
  {text: 'Cortar la novia', complete: false},
  {text: 'Cortar la luz del vecino', complete: true},
];

function App() {
  return (
    <>
      <ToDoTitle />
      <ToDoCount completed={3} total={12}/>
      <ToDoSearch />
      <ToDoList>
        {arrayToDos.map(todo => (
          <ToDoItem key={todo.text} text={todo.text} status={todo.complete}/>
        ))}
      </ToDoList>

      <BtnToDo />
    </>
  );
}

export default App;
