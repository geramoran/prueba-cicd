import React from 'react'

function ToDoCount({total , completed}) {
  return (
    <h3>
        Has completado {completed} de {total} tareas
    </h3>
  )
}

export {ToDoCount}
