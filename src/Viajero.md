Viajero
- Total
- Movimientos

Viaje
- [Viajero]

Movimientos
- Descripcion
- Costo

Objetivo: Saber el costo total de cada viajero y cuanto pagar a cada uno por sus viaticos

- Crear una lista de viajeros
- Cada viajero va a introducir un movimiento
- Ese movimiento sera restado a los demas viajeros
- Cada viajero podra saber cuanto lleva gastado y cuanto debe a cada quien
- Podra saber si un viajero ya pago a otro viajero de sus movimientos